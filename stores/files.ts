import { defineStore } from 'pinia';
export type IFile = {
  phoneNumber: string, 
  userName: string, 
  gender: string,
  nation?: string,
  email?: string,
  bloodGroup?:string
}
export type ICurrentFile = {
    id: number, 
    file: IFile,
    dob: string, 
    address: string, 
    mainFile: string, 
    notify: string, 
    action: boolean
}

interface IState {
  currentFile: ICurrentFile
  tableData: ICurrentFile[],
  currentIndex: number
}

export const useFileStore = defineStore('File', {
  state: (): IState => {
    return {
      currentFile: {
        id: 0,
        file: { 
          phoneNumber: '', 
          userName: '', 
          gender: '',
          email: '',
          bloodGroup: '',
          nation: ''
        }, 
        dob: '', 
        address: '', 
        mainFile: '', 
        notify: '', 
        action: false
      },
      tableData: [],
      currentIndex: 0,
    };
  },
  actions: {
    setCurrentFile(file:ICurrentFile) {
      this.currentFile = file
    },
    setTableData(data: ICurrentFile[]) {
      this.tableData = data
    },
    addNewData(data: ICurrentFile) {
			this.tableData.unshift(data)
			localStorage.setItem('tableData', JSON.stringify(this.tableData))
    },
    setCurrentIndex(index:number) {
      this.currentIndex = index
    }
  },
});
