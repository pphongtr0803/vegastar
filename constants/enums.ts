const listGender = [
  {
    value: "default",
    title: "- Chọn giới tính - ",
  },
  {
    value: "Nam",
    title: "Nam",
  },
  {
    value: "Nữ",
    title: "Nữ",
  },
  {
    value: "Khác",
    title: "Khác",
  },
]
const listBlood = [
  {
    value: "default",
    title: "- Chọn nhóm máu - ",
  },
  {
    value: "A",
    title: "Nhóm máu A",
  },
  {
    value: "B",
    title: "Nhóm máu B",
  },
  {
    value: "AB",
    title: "Nhóm máu AB",
  },
  {
    value: "O",
    title: "Nhóm máu O",
  },
]
const listNations = [
  {
    value: "default",
    title: "- Chọn quốc gia - ",
  },
  {
    value: "VN",
    title: "Việt Nam",
  },
  {
    value: "US",
    title: "Mỹ",
  },
  {
    value: "UK",
    title: "Anh",
  },
  {
    value: "JP",
    title: "Nhật Bản",
  },
  {
    value: "KR",
    title: "Hàn Quốc",
  },
];

const tableData = [
  { id: 1, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 2, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 3, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 4, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 5, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 6, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 7, file: { phoneNumber: '0123', userName: 'Phong', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },
  { id: 8, file: { phoneNumber: '0123', userName: 'Phong1', gender: 'Nam', nation: 'VN', email: 'pphongtr0803@gmail.com', bloodGroup: 'A' }, dob: '2000-03-08', address: 'HN', mainFile: 'abc', notify: 'Green', action: false },

]

const labels = [
  { text: 'STT', field: 'count' },
  { text: 'Hồ sơ', field: 'file'  },
  { text: 'Ngày sinh', field: 'dob'  },
  { text: 'Địa chỉ', field: 'address' },
  { text: 'Hồ sơ chính', field: 'mainFile' },
  { text: 'Thông báo', field: 'notify'},
  { text: '', field: 'action'}
]
export { listGender, listNations, listBlood, tableData, labels };
