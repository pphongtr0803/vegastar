// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  postcss: {
    plugins: {
      autoprefixer: {},
    },
  },
  css: [
    '~/assets/css/reset.css',
    '@fortawesome/fontawesome-free/css/all.min.css',
  ],
  modules: [
    '@pinia/nuxt',
  ],
})
